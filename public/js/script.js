(function($) {
    "use strict"; // Start of use strict
    // Toggle the side navigation
    $(document).on('click', '.sidenavToggler', function(e) {
        e.preventDefault();
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
        $("#search").focus();
        $("#search-icon").toggleClass("disabled-link");
    });
    // Force the toggled class to be removed when a collapsible nav link is clicked
    $(document).on('click', '.navbar-sidenav .nav-link-collapse', function(e) {
        e.preventDefault();
        $("body").removeClass("sidenav-toggled");
        $("#search-icon").removeClass("disabled-link");
    });
    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
    });
    // Configure tooltips globally
    $("body").tooltip({
        selector: '[data-toggle="tooltip"]',
        trigger : 'hover'
    });
    // Sliders on Sidebar Nav
    $("#slider1").slider({});
    $("#slider2").slider({});
    $("#slider3").slider({});
    $("#slider4").slider({});
    $("#slider5").slider({});
    $("#slider6").slider({});
    // Province details open/close
    $(document).on('click', '#closeDetails', function(e) {
        e.preventDefault();
        $(".province-details-container").hide();
        $(".philippine-map").removeClass("col-9 col-md-10 col-lg-6").addClass("col");
    });
    $(document).on('click', '.show-details', function(e) {
        e.preventDefault();
        $(".province-details-container").show();
        $(".philippine-map").removeClass("col").addClass("col-9 col-md-10 col-lg-6");
    });
    // Add customClass option to Bootstrap Tooltip
    var Tooltip = $.fn.tooltip.Constructor;
    $.extend( Tooltip.Default, {
        customClass: ''
    });
    var _show = Tooltip.prototype.show;
    Tooltip.prototype.show = function () {
    _show.apply(this,Array.prototype.slice.apply(arguments));
        if ( this.config.customClass ) {
            var tip = this.getTipElement();
            $(tip).addClass(this.config.customClass);
        }
    };
    // Custom content for province map tooltip
    $('svg #PH-NEC_1_').tooltip({
        customClass: 'map-tooltip',
        title: '<h4 class="text-left bg-faded-green mb-0">Negros Occidental</h4><div class="text-center"><img class="img-fluid d-block mx-auto" src="images/talent.png" alt=""><img class="img-fluid d-block mx-auto" src="images/infra.png" alt=""></div>',
        html: true,
        placement: 'top',
        container: 'body'
    });
    $('svg #PH-NER_1_').tooltip({
        customClass: 'map-tooltip',
        title: '<h4 class="text-left bg-teal mb-0">Negros Oriental</h4><div class="text-center"><img class="img-fluid d-block mx-auto" src="images/talent.png" alt=""><img class="img-fluid d-block mx-auto" src="images/infra.png" alt=""></div>',
        html: true,
        placement: 'bottom',
        container: 'body'
    });
    $('svg #PH-CEB_1_').tooltip({
        customClass: 'map-tooltip',
        title: '<h4 class="text-left bg-blue mb-0">Cebu</h4><div class="text-center"><img class="img-fluid d-block mx-auto" src="images/talent.png" alt=""><img class="img-fluid d-block mx-auto" src="images/infra.png" alt=""></div>',
        html: true,
        placement: 'top',
        container: 'body'
    });
    $('svg #PH-BOH_1_').tooltip({
        customClass: 'map-tooltip',
        title: '<h4 class="text-left bg-light-green mb-0">Bohol</h4><div class="text-center"><img class="img-fluid d-block mx-auto" src="images/talent.png" alt=""><img class="img-fluid d-block mx-auto" src="images/infra.png" alt=""></div>',
        html: true,
        placement: 'bottom',
        container: 'body'
    });

})(jQuery);

$(document).ready(function() {
    // Carousel script for Province Details
    $('#carouselContainer').carousel({
        interval: 4000
    })

    $("#carouselContainer").on("slide.bs.carousel", function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $(".carousel-item").length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
              // append slides to end
              if (e.direction == "left") {
                $(".carousel-item")
                  .eq(i)
                  .appendTo(".carousel-inner");
              } else {
                $(".carousel-item")
                  .eq(0)
                  .appendTo($(this).find(".carousel-inner"));
              }
            }
        }
    });

    // Ajax search function for provinces
    $.ajaxSetup({ cache: false });
    $('#search').keyup(function() {
        $('.tooltip').hide();
        $('#result').html('');
        var searchField = $('#search').val();
        var expression = new RegExp(searchField, "i");
        $.getJSON('province.json', function(data) {
            $.each(data, function(key, value){
                if (value.name.search(expression) != -1 || value.id.search(expression) != -1 || value.acronym.search(expression) != -1) {
                    $('#result').append('<li class="province-list show-details" data-toggle="tooltip" data-placement="right" title="' +value.name+ '"><span class="province-number ' +value.color+ ' text-center"><small>' +value.id +'</small></span><span class="province-text text-center">' + value.acronym +'</span><span class="province-text-whole">' + value.name +'</span></li>');
                }
            });   
        });
    });
});